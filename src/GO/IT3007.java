package GO;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IT3007 extends JFrame{

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IT3007 window = new IT3007();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IT3007() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(189, 183, 107));
		frame.setBounds(100, 100, 704, 496);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btn0 = new JButton("0");
		btn0.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn0.setBounds(238, 121, 60, 59);
		frame.getContentPane().add(btn0);
		
		JButton btn1 = new JButton("1");
		btn1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn1.setBounds(293, 121, 60, 59);
		frame.getContentPane().add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn2.setBounds(351, 121, 60, 59);
		frame.getContentPane().add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn3.setBounds(406, 121, 60, 59);
		frame.getContentPane().add(btn3);
		
		JButton btn4 = new JButton("4");
		btn4.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn4.setBounds(238, 178, 60, 59);
		frame.getContentPane().add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn5.setBounds(293, 178, 60, 59);
		frame.getContentPane().add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn6.setBounds(351, 178, 60, 59);
		frame.getContentPane().add(btn6);
		
		JButton btn7 = new JButton("7");
		btn7.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn7.setBounds(406, 178, 60, 59);
		frame.getContentPane().add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn8.setBounds(293, 236, 60, 59);
		frame.getContentPane().add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn9.setBounds(351, 236, 60, 59);
		frame.getContentPane().add(btn9);
		
		textField = new JTextField();
		textField.setBounds(51, 23, 575, 68);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblnum = new JLabel(" insert 10 numbers");
		lblnum.setBackground(Color.CYAN);
		lblnum.setForeground(new Color(255, 0, 0));
		lblnum.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblnum.setBounds(31, 128, 166, 48);
		frame.getContentPane().add(lblnum);
		
		JButton btnSort = new JButton("sort");
		btnSort.setBackground(Color.WHITE);
		btnSort.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSort.setBounds(31, 382, 85, 42);
		frame.getContentPane().add(btnSort);
		
		JButton btnSearch = new JButton("search");
		btnSearch.setBackground(Color.WHITE);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSearch.setBounds(555, 395, 85, 42);
		frame.getContentPane().add(btnSearch);
	}
}

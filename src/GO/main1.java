package GO;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class main1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main1 frame = new main1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 757, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnBTree = new JButton("B+ Tree");
		btnBTree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				B_plus_tree obj = new B_plus_tree();
				obj.setVisible(true);	
			}
		});
		btnBTree.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnBTree.setBounds(25, 31, 200, 76);
		contentPane.add(btnBTree);
		
		JButton btnQuickSort = new JButton("Quick Sort");
		btnQuickSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Sort obj2 = new Sort();
				obj2.setVisible(true);
			}
		});
		btnQuickSort.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnQuickSort.setBounds(25, 141, 200, 76);
		contentPane.add(btnQuickSort);
		
		JButton btnLinear = new JButton("Linear ");
		btnLinear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Search obj3 = new Search();
				obj3.setVisible(true);
			}
		});
		btnLinear.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnLinear.setBounds(25, 259, 200, 76);
		contentPane.add(btnLinear);
		
		JButton btnNewButton = new JButton("Exit");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(632, 415, 101, 44);
		contentPane.add(btnNewButton);
	}
}

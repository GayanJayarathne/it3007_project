import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.border.Border;

public class Search extends JFrame{
	
	Integer SingleSearchKeyInput=0,RangeSearchUpKeyInput=0,RangeSearchLowKeyInput=0;

	JFrame frame;
	private JTextField textField;
	private JTextField textField2;
	private JTextField textField3;
	private JTextArea textArea;
	private JLabel resultsNo;
	
	 static ArrayList<Double> receivedValues = new ArrayList<Double>();
	 static ArrayList<Integer> receivedKeys = new ArrayList<Integer>();
	 static Double gotVal;
	 static Integer gotKey;
	 String resultsSet="";
	 String resultsKeys="";
	 String result="";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Search window = new Search();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
	
	/**
	 * Create the application.
	 */
	public Search() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	void initialize() {
		frame = new JFrame();
		frame.setSize(800,640);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		  try {
	            frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("src/background1.png")))));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        //frame.pack();
		  
		  JLabel Topic = new JLabel("Searching....");
		  Topic.setFont(new Font("Arial", Font.BOLD, 60));
		  Topic.setForeground(Color.yellow);
		  Topic.setBounds(100, 10, 400, 100);
		  
		  JLabel label = new JLabel();
		  label.setIcon(new ImageIcon(new ImageIcon("src/search.gif").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT)));
		  label.setBounds(660, 10, 100, 100); // You can use your own values
		  frame.getContentPane().add(label);
		  
		
		
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(new Color(0,0,0,40));
		leftPanel.setBounds(20, 150, 380, 220);
		//leftPanel.add(lblNewLabel_1);
		leftPanel.setVisible(true);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setBackground(new Color(0,0,0,40));
		rightPanel.setBounds(400, 150, 360, 220);
		//leftPanel.add(lblNewLabel_1);
		rightPanel.setVisible(true);
		
		JPanel resultsPanel = new JPanel();
		 resultsPanel.setBackground(new Color(0,0,0,0));
		 resultsPanel.setBounds(20, 380, 740, 150);
		//leftPanel.add(lblNewLabel_1);
		 resultsPanel.setVisible(true);
		
		
		 textArea = new JTextArea(740, 300);
		 textArea.setBorder(new EmptyBorder(10,10,10,10));
		 textArea.setBackground(Color.black);
		 textArea.setForeground(Color.white);
		 
		JScrollPane scrPane = new JScrollPane(textArea);
		scrPane.setPreferredSize(new Dimension(740,100));
		scrPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrPane.setBackground(Color.black);
		//scrPane.setBounds(20, 360, 740, 200);
		scrPane.setVisible(true);
		
		JLabel leftTopic = new JLabel("Single Search");
		leftTopic.setFont(new Font("Arial", Font.BOLD, 32));
		leftTopic.setForeground(Color.white);
		leftTopic.setBorder(new EmptyBorder(10,0,5,0));
		
		JLabel rightTopic = new JLabel("Range Search");
		rightTopic.setFont(new Font("Arial", Font.BOLD, 32));
		rightTopic.setForeground(Color.white);
		rightTopic.setBorder(new EmptyBorder(10,0,5,0));
		
		JLabel resultsTopic = new JLabel("Search Results");
		resultsTopic.setFont(new Font("Arial", Font.BOLD, 32));
		resultsTopic.setForeground(Color.yellow);
		
		resultsNo = new JLabel("");
		resultsNo.setFont(new Font("Arial", Font.BOLD, 18));
		resultsNo.setForeground(Color.green);
		resultsNo.setBounds(320, 540, 200, 40);
		
		JLabel lblNewLabel_1 = new JLabel("Enter KEY Value to Search");
		lblNewLabel_1.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1.setForeground(Color.yellow);
		lblNewLabel_1.setBorder(new EmptyBorder(5,0,10,0));
		
		
		JLabel lblNewLabel_2 = new JLabel("Enter KEY Ranges to Search");
		lblNewLabel_2.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_2.setForeground(Color.yellow);
		lblNewLabel_2.setBorder(new EmptyBorder(5,0,10,0));
		 
	

		JButton btnNewButton = new JButton("Search");
		btnNewButton.setBackground(new Color(135, 206, 235));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searching();
			}

			
		});
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 22));
		btnNewButton.setBounds(320,320, 150, 40);
				
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField.setBorder(new EmptyBorder(5,5,5,5));
		textField.setColumns(13);
		textField.setText("");
		
		
		textField2 = new JTextField();
		textField2.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField2.setBorder(new EmptyBorder(5,5,5,5));
		textField2.setColumns(6);
		textField2.setText("");
		
		textField3 = new JTextField();
		textField3.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField3.setBorder(new EmptyBorder(5,5,5,5));
		textField3.setColumns(6);
		textField3.setText("");
		
		
		JLabel lbel_Display_Result = new JLabel("index=4");
		lbel_Display_Result.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lbel_Display_Result.setForeground(Color.black);
		lbel_Display_Result.setBounds(182, 236, 151, 41);
		
		JLabel input1Label = new JLabel("Single KEY to search");
		input1Label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		input1Label.setForeground(Color.white);
		input1Label.setBorder(new EmptyBorder(0,50,0,50));
		
		
		JLabel input2Label = new JLabel("Lower Bound KEY");
		input2Label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		input2Label.setForeground(Color.white);
		input2Label.setBorder(new EmptyBorder(0,47,0,0));
		input2Label.setPreferredSize(new Dimension(150,15));
		
		
		JLabel input3Label = new JLabel("Upper Bound KEY");
		input3Label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		input3Label.setForeground(Color.white);
		input3Label.setBorder(new EmptyBorder(0,5,0,0));
		input3Label.setPreferredSize(new Dimension(150,15));
		
		 JButton goToInsert = new JButton("Back To Insert");
		 goToInsert.setBackground(new Color(135, 206, 235));
		 goToInsert.setForeground(Color.BLACK);
		 goToInsert.setFont(new Font("Verdana", Font.BOLD, 22));
		 goToInsert.setBounds(20,540, 240, 40);
		 goToInsert.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.setVisible(false);
					Inserting_and_Sorting.frame2.setVisible(true);	
				}
			});

	
		leftPanel.add(leftTopic);
		leftPanel.add(lblNewLabel_1);
		leftPanel.add(textField);
		leftPanel.add(input1Label);
		
		rightPanel.add(rightTopic);
		rightPanel.add(lblNewLabel_2);
		rightPanel.add(textField2);
		rightPanel.add(textField3);
		rightPanel.add(input2Label);
		rightPanel.add(input3Label);
		
		frame.add(Topic);
		frame.getContentPane().add(btnNewButton);
		frame.getContentPane().add(leftPanel);
		frame.getContentPane().add(rightPanel);
		
		resultsPanel.add(resultsTopic);
		resultsPanel.add(scrPane);
		//resultsPanel.add(resultsNo);
		frame.getContentPane().add(resultsPanel);
		frame.add(resultsNo);
		frame.add(goToInsert);

		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void searching() {
		//Checking whether only one option is selected
		if(((!textField.getText().isEmpty()) && ((textField2.getText().isEmpty()) && (textField3.getText().isEmpty()))) ||
			((textField.getText().isEmpty()) && ((!textField2.getText().isEmpty()) && (!textField3.getText().isEmpty())))) {
				
			BPlusTree bpt1 = Inserting_and_Sorting.bpt;
			
			//Checking whether single search is valid
			if((!textField.getText().isEmpty()) && ((textField2.getText().isEmpty()) && (textField3.getText().isEmpty()))) {
				
				try {
				     SingleSearchKeyInput=Integer.parseInt(textField.getText());
				    
				     //BPlusTree bpt1 = Sorting.bpt;
				     //bpt = new BPlusTree(3);
				     //bpt.insert(5, 33);
				     //bpt.insert(15, 21);
				     //bpt.insert(25, 31);
				     //bpt.insert(35, 41);
				     //bpt.insert(45, 10);
				     
				     bpt1.search(SingleSearchKeyInput);
				     if(bpt1.search(SingleSearchKeyInput) != null) {
				    	 
				    	 textArea.setText("KEY  :  "+gotKey+"	"+"Value  :  "+gotVal);
				    	 resultsNo.setText("1 Results Found");
				    	 textField.setText("");
				     }
				     
				     else {
				    	 textArea.setText("No Result Found");
				    	 resultsNo.setText("0 Results Found");
				     }
				          
				}
				catch (NumberFormatException e) {
				     //Not an integer
					System.out.println("Non numerical Input");
				}
		
		}
			//Checking whether range search is valid
			else if((textField.getText().isEmpty()) && ((!textField2.getText().isEmpty()) && (!textField3.getText().isEmpty()))) {
				
				resultsNo.setText("");
			
				try {
				     RangeSearchUpKeyInput=Integer.parseInt(textField3.getText());
				     RangeSearchLowKeyInput=Integer.parseInt(textField2.getText());
				     
				     //BPlusTree bpt = null;
				     //bpt = new BPlusTree(5);
				     //bpt.insert(5, 33);
				     //bpt.insert(15, 21);
				     //bpt.insert(25, 31);
				     //bpt.insert(35, 41);
				     //bpt.insert(45, 10); 
				     bpt1.search(RangeSearchLowKeyInput,RangeSearchUpKeyInput);
				     int i=receivedKeys.size();
				     if(i>0) {
				    	 
					     for (int j = 0; j <i; j++) {
					    	result =result+"KEY  :  "+String.valueOf(receivedKeys.get(j))+"	"+"Value  :  "+String.valueOf(receivedValues.get(j))+"\n"; 
					     }
					     textArea.setText(result);
					     result="";
					     resultsNo.setText(i+"  Results Found");
					     textField2.setText("");
					     textField3.setText("");
				    }
				     else if(i==0) {
				    	 textArea.setText("No Result Found");
				    	 resultsNo.setText("0 Results Found");
				     }
				     	 
				}
				catch (NumberFormatException e) {
				     //Not an integer
					System.out.println("Non numerical Input");
				}
				
			}
		}
	}
}


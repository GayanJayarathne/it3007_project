import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.border.Border;

import java.time.Duration;
import java.time.Instant;

public class Inserting_and_Sorting extends JFrame{
	
	static Integer InputKey=0;

	static Integer InputValue=0;

	static JFrame frame2;
	private static JTextField textField;
	private static JTextField textField2;
	private static JTextField textField3;
	private static JTextArea textArea;
	private static JLabel resultsNo;
	private static JLabel sortTime;
	private static JComboBox c1;
	
	
	 static ArrayList<Double> allValues = new ArrayList<Double>();
	 static ArrayList<Integer>allKeys = new ArrayList<Integer>();
	 
	 //static Integer[] arr1;
	 
	 
	 static Double gotVal;
	 static Integer gotKey;
	 String resultsSet="";
	 String resultsKeys="";
	 static String sortresult="";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initialize();
	}
	
	
	/**
	 * Create the application.
	 */
	public Inserting_and_Sorting() {
		initialize();
	}

	
	//Making window interface
	private static void initialize() {
		frame2 = new JFrame();
		frame2.setSize(800,640);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.getContentPane().setLayout(null);
		
		  try {
	            frame2.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("src/background1.png")))));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	      
		  
		  JLabel Topic = new JLabel("Inserting & Sorting");
		  Topic.setFont(new Font("Arial", Font.BOLD, 60));
		  Topic.setForeground(Color.yellow);
		  Topic.setBounds(30, 10, 550, 100);
		  
		  JLabel label = new JLabel();
		  label.setIcon(new ImageIcon(new ImageIcon("src/sorting.gif").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT)));
		  label.setBounds(660, 10, 100, 100);
		  frame2.getContentPane().add(label);
		  
		
		
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(new Color(0,0,0,40));
		leftPanel.setBounds(20, 120, 380, 200);
		//leftPanel.add(lblNewLabel_1);
		leftPanel.setVisible(true);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setBackground(new Color(0,0,0,40));
		rightPanel.setBounds(400, 120, 360, 200);
		//leftPanel.add(lblNewLabel_1);
		rightPanel.setVisible(true);
		
		JPanel resultsPanel = new JPanel();
		 resultsPanel.setBackground(new Color(0,0,0,0));
		 resultsPanel.setBounds(20, 340, 740, 170);
		//leftPanel.add(lblNewLabel_1);
		 resultsPanel.setVisible(true);
		
		
		 textArea = new JTextArea(740, 100);
		 textArea.setBorder(new EmptyBorder(10,10,10,10));
		 textArea.setBackground(Color.black);
		 textArea.setForeground(Color.white);
		 
		JScrollPane scrPane = new JScrollPane(textArea);
		scrPane.setPreferredSize(new Dimension(740,120));
		scrPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrPane.setBackground(Color.black);
		scrPane.setVisible(true);
		
		JLabel leftTopic = new JLabel("KEY Insertion");
		leftTopic.setFont(new Font("Arial", Font.BOLD, 32));
		leftTopic.setForeground(Color.white);
		leftTopic.setBorder(new EmptyBorder(10,0,5,0));
		
		JLabel rightTopic = new JLabel("VALUE Insertion");
		rightTopic.setFont(new Font("Arial", Font.BOLD, 32));
		rightTopic.setForeground(Color.white);
		rightTopic.setBorder(new EmptyBorder(10,0,5,0));
		
		JLabel resultsTopic = new JLabel("Sorted Data Results");
		resultsTopic.setFont(new Font("Arial", Font.BOLD, 28));
		resultsTopic.setForeground(Color.yellow);
		resultsTopic.setBorder(new EmptyBorder(0,0,0,80));
		
		JLabel method = new JLabel("Quick Sort Method : ");
		method.setFont(new Font("Arial", Font.BOLD, 24));
		method.setForeground(Color.yellow);
		
		resultsNo = new JLabel("");
		resultsNo.setFont(new Font("Arial", Font.BOLD, 18));
		resultsNo.setForeground(Color.green);
		resultsNo.setBounds(320, 550, 200, 40);
		
		JLabel lblNewLabel_1 = new JLabel("Enter KEY to Insert");
		lblNewLabel_1.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1.setForeground(Color.yellow);
		lblNewLabel_1.setBorder(new EmptyBorder(5,0,10,0));
		
		
		JLabel lblNewLabel_2 = new JLabel("Enter VALUE to Insert");
		lblNewLabel_2.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_2.setForeground(Color.yellow);
		lblNewLabel_2.setBorder(new EmptyBorder(5,0,10,0));
		
		sortTime = new JLabel("");
		sortTime.setFont(new Font("Verdana", Font.BOLD, 14));
		sortTime.setForeground(Color.yellow);
		sortTime.setBackground(Color.black);
		sortTime.setBounds(570,530, 180, 40);
		
		
		String s1[] = { "Last Pivot", "First Pivot", "Random Pivot", "Mean Pivot"};
		c1 = new JComboBox(s1);
		c1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	Integer index=c1.getSelectedIndex();
                
            }
        });
		

		JButton btnNewButton = new JButton("INSERT");
		btnNewButton.setBackground(new Color(135, 206, 235));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inserting();
			}

			
		});
			
		JButton btnNewButton2 = new JButton("SORT");
		btnNewButton2.setBackground(new Color(135, 206, 235));
		btnNewButton2.setForeground(Color.BLACK);
		btnNewButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quickSort();
			}
		});

		
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 22));
		btnNewButton.setBounds(320,270, 150, 40);
		
		btnNewButton2.setFont(new Font("Verdana", Font.BOLD, 22));
		btnNewButton2.setBounds(320,530, 150, 40);
				
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField.setBorder(new EmptyBorder(5,5,5,5));
		textField.setColumns(13);
		textField.setText("");
		
		
		textField2 = new JTextField();
		textField2.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField2.setBorder(new EmptyBorder(5,5,5,5));
		textField2.setColumns(13);
		textField2.setText("");
		
				
		JLabel lbel_Display_Result = new JLabel("index=4");
		lbel_Display_Result.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lbel_Display_Result.setForeground(Color.black);
		lbel_Display_Result.setBounds(182, 236, 151, 41);
		
		Icon icon2 = new ImageIcon("src/magnifier.png");
	    JButton goToSearch = new JButton(icon2);
	    goToSearch.setToolTipText("Go to Search");
	    //goToSearch.setBounds(50,520, 150, 100);
	    goToSearch.setBounds(20, 520, 60, 60);
	    try{
	        Image image = ImageIO.read(new File("src/magnifier.png")).getScaledInstance(60, 60, Image.SCALE_DEFAULT);
	        goToSearch.setIcon(new ImageIcon(image));
	    } 
	    catch (Exception e) {
	    }
	    
	    goToSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame2.setVisible(false);
				Search Object1 = new Search();
				Object1.frame.setVisible(true);
				
			}
		});
		
		leftPanel.add(leftTopic);
		leftPanel.add(lblNewLabel_1);
		leftPanel.add(textField);
		
		
		rightPanel.add(rightTopic);
		rightPanel.add(lblNewLabel_2);
		rightPanel.add(textField2);
		
		
		frame2.add(Topic);
		frame2.getContentPane().add(btnNewButton);
		frame2.getContentPane().add(leftPanel);
		frame2.getContentPane().add(rightPanel);
		
		resultsPanel.add(resultsTopic);
		resultsPanel.add(method);
		resultsPanel.add(c1);
		resultsPanel.add(scrPane);
		//resultsPanel.add(resultsNo);
		frame2.getContentPane().add(resultsPanel);
		frame2.getContentPane().add(btnNewButton2);
		frame2.add(sortTime);
		frame2.add(goToSearch);
		
		//frame2.getContentPane().add(panel, BorderLayout.CENTER);
		frame2.setResizable(false);
		frame2.setVisible(true);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	
	static BPlusTree bpt=new BPlusTree(3);
	
	private static void inserting() {
		
		//Checking whether Key and Value are not empty
		if((!textField.getText().isEmpty()) && (!textField2.getText().isEmpty())) {
			
			try {
				//pass two inputs in to integers 
				InputKey=Integer.parseInt(textField.getText());
				InputValue=Integer.parseInt(textField2.getText());
				 
			    //check whether Key is available
				if(bpt.search(InputKey) == null) {
					
					//Insert data into B+ tree 
					bpt.insert(InputKey, InputValue);
					
					//Retrieve all data in B+ tree (from index 0 to index 1000)
				    bpt.search(0,1000);
				    
				    //Show a message when inserted
				    JOptionPane.showMessageDialog(frame2, "Data Insertion Successful!");
				    textField.setText("");
				    textField2.setText("");
				
				}
				else {
					JOptionPane.showMessageDialog(frame2, "KEY already used!");
					
				}
			     
			}
			catch(NumberFormatException e){
				
				//Show a message when inputs are not numeric
				JOptionPane.showMessageDialog(frame2, "Accept only Numeric KEYS and VALUES");
			}
		}
		else {
			
			//Show a message when inputs are empty
			JOptionPane.showMessageDialog(frame2, "Empty Fields Present");
		}
	}
	
	private static void quickSort() {
		
		//Get amount of data retrieve
		int n=allKeys.size();
		
		if(n>0) {
			
			//Create a QuickSort object	
			QuickSort Obj1 = new QuickSort();
			
			//Set key values of retrieved data to a int array 
			int[] keys = allKeys.stream().mapToInt(i->i).toArray();
			
			//Get time before start QuickSort
			long start = System.nanoTime();
			
			//Sort data with QuickSort Algorithm
			Obj1.quickSort(keys, 0, n - 1);
			
			//Get time after QuickSort
			long end = System.nanoTime();   
			
			//Get sorted data
			Obj1.printArray(keys, n);
			
			//Set time spend to sorting
			sortTime.setText("Sorting Time : "+String.valueOf(end-start)+" ns");
				
			//Display sorted data one by one     
			for (int j = 0; j <n; j++) {
			    	sortresult =sortresult+"KEY  :  "+String.valueOf(keys[j])+"	"+"Value  :  "+String.valueOf(allValues.get(j))+"\n"; 
			     }
			     textArea.setText(sortresult);
			     sortresult="";
	    }
		else {
			//Show no data to sort
			JOptionPane.showMessageDialog(frame2, "No Data to Sort!");
		}
	}
}



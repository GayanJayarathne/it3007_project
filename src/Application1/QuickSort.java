package Application1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;

//import quickSort.QuickSort;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.ImageIcon;

public class QuickSort {

	private JFrame frame;
	private JTextField textField_1;
	static JLabel lblNewLabel_1;
	static JTextArea textArea;
	String array1;
//    double index;
    private int length;
    private static int array[];
    private static int sortedArray[];
    static String sortresult="";
    String list;
//    private int high;
//    private int n;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QuickSort window = new QuickSort();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
//		int n = array.length;
//			
//		quickSort(array, 0, n - 1);
//		System.out.println("Sorted array: ");
//		printArray(array, n);
//		
		
	}

	/**
	 * Create the application.
	 */
	public QuickSort() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 822, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Enter Number list :");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(24, 39, 249, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Enter");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				array1 = textField_1.getText();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnNewButton.setBounds(516, 174, 146, 51);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Sort");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				list = textField_1.getText();

				
				// String to String array
		        String[] strArray = null; 
		        strArray = list.split(",");
		        
		        // String array to integer array
		        int size = strArray.length;
		        int [] arr = new int [size];
		        for(int i=0; i<size; i++) {
		            arr[i] = Integer.parseInt(strArray[i]);
		        }		     
		        quickSort(arr, 0, size - 1);
		       		        
		        textArea.setText("Entered list is : " + list + "\n" + "Sorted array:  : " + Arrays.toString(arr));

			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnNewButton_1.setBounds(306, 268, 178, 60);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Exit");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_2.setBounds(700, 454, 98, 39);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_2_1 = new JButton("Back");
		btnNewButton_2_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_2_1.setBounds(592, 454, 98, 39);
		frame.getContentPane().add(btnNewButton_2_1);
		
		JButton btnNewButton_2_2 = new JButton("Reset");
		btnNewButton_2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_1.setText("");
				textArea.setText("");
			}
		});
		btnNewButton_2_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_2_2.setBounds(20, 454, 98, 39);
		frame.getContentPane().add(btnNewButton_2_2);
		
		textField_1 = new JTextField();
		textField_1.setBounds(130, 88, 533, 76);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textArea = new JTextArea();
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(Color.WHITE);
		textArea.setBounds(178, 356, 431, 76);
		frame.getContentPane().add(textArea);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(59, 197, 342, 51);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("D:\\background1.png"));
		lblNewLabel_2.setBounds(0, 0, 808, 503);
		frame.getContentPane().add(lblNewLabel_2);
	}
    
	static void swap(int[] arr, int i, int j)
	{
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	/* This function takes last element as pivot, places
	the pivot element at its correct position in sorted
	array, and places all smaller (smaller than pivot)
	to left of pivot and all greater elements to right
	of pivot */
	static int partition(int[] arr, int low, int high)
	{
		
		// pivot
		int pivot = arr[high];
		
		// Index of smaller element and
		// indicates the right position
		// of pivot found so far
		int i = (low - 1);

		for(int j = low; j <= high - 1; j++)
		{
			// If current element is smaller
			// than the pivot
			if (arr[j] < pivot)
			{
				
				// Increment index of
				// smaller element
				i++;
				swap(arr, i, j);
			}
		}
		swap(arr, i + 1, high);
		return (i + 1);
	}
}



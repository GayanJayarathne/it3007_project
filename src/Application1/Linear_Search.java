package Application1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import GO.Search;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;

public class Linear_Search {

	private JFrame frame;
	private JTextField textField_1;
    private JTextField textField;
	private JLabel lblNewLabel;
	private JTextArea textArea;
	String list;
    Double index;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Linear_Search window = new Linear_Search();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Linear_Search() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 536, 404);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("MingLiU_HKSCS-ExtB", Font.BOLD, 17));
		textArea.setBounds(77, 279, 325, 75);
		textArea.setBorder(new EmptyBorder(10,10,10,10));
		textArea.setBackground(Color.black);
		textArea.setForeground(Color.white);
		frame.getContentPane().add(textArea);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.setBackground(new Color(135, 206, 235));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				index = Double.parseDouble(textField.getText());
				textField.setText("");
		        
		        // String to String array
		        String[] strArray = null; 
		        strArray = list.split(",");
		        
		        // String array to integer array
		        int size = strArray.length;
		        int [] arr = new int [size];
		        for(int i=0; i<size; i++) {
		            arr[i] = Integer.parseInt(strArray[i]);
		        }		        
		        textArea.setText("Entered list is : " + list + "\n" + "searched value  : " + index + "\n" + index + " is found at index : " + search(arr, index));
			}
		});
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 15));
		btnNewButton.setBounds(416, 224, 94, 29);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Enter Number You want to Search :");
		lblNewLabel_1.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(8, 172, 325, 41);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Enter Number list : ");
		lblNewLabel_1_2.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1_2.setForeground(Color.WHITE);
		lblNewLabel_1_2.setBounds(8, 52, 176, 41);
		frame.getContentPane().add(lblNewLabel_1_2);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField_1.setColumns(10);
		textField_1.setBounds(56, 91, 289, 41);
		frame.getContentPane().add(textField_1);
		
		JButton btnNewButton_1 = new JButton("Enter");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				list = textField_1.getText();
				textField_1.setText("");
			}
		});
		btnNewButton_1.setForeground(Color.BLACK);
		btnNewButton_1.setFont(new Font("Verdana", Font.BOLD, 15));
		btnNewButton_1.setBackground(new Color(135, 206, 235));
		btnNewButton_1.setBounds(416, 103, 94, 29);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Linear Search");
		lblNewLabel_1_1.setForeground(Color.BLACK);
		lblNewLabel_1_1.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1_1.setBounds(200, 0, 220, 41);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.BOLD, 17));
		textField.setColumns(10);
		textField.setBounds(334, 172, 133, 41);
		frame.getContentPane().add(textField);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Search.class.getResource("background1.png")));
		lblNewLabel.setBounds(0, 42, 520, 333);
		frame.getContentPane().add(lblNewLabel);		
	}
	
	public static int search(int arr[], Double x){
        int n = arr.length;
        
        for (int i = 0; i <= n; i++){
            if (arr[i]==x){
                return i;
            }
        }
        return -1;      
    }
}
